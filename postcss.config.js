module.exports = {
    options: { sourceMap: true },
    plugins: [
        require('autoprefixer'),
        require('cssnano')({
            preset: 'default',
        }),
    ]
}