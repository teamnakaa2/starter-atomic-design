var path = require('path');
 var webpack = require('webpack');
 const MiniCssExtractPlugin = require('mini-css-extract-plugin');
 const IgnoreEmitPlugin = require('ignore-emit-webpack-plugin');

 const { CleanWebpackPlugin } = require('clean-webpack-plugin');

 module.exports = {
     mode: 'development',
     devtool:  (process.env.NODE_ENV == 'production') ? 'inline-source-map' : 'inline-source-map',
     entry: {
       'index' : ['./atomic-design.js'],
       'style' : ['./styles/atomic-design.scss']
     },
     optimization: {
        minimize: false
    },
     output: {
        path: path.resolve('./www/assets/'),
        filename: '[name].js',
     },
     module: {
        rules: [
            { test: /\.js$/, exclude: /node_modules/,
              use: {
                loader: 'babel-loader',
                options: {
                  presets: ['@babel/preset-env']
                }
              }
            },
          { 
            test: /\.scss$/, 
            use: [
            MiniCssExtractPlugin.loader,
            { loader: 'css-loader', options: { sourceMap: true } },
            { loader: 'postcss-loader', options: { sourceMap: true } },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: true,
                sassOptions: {
                  indentWidth: 4,
                },
              },
            },
            'import-glob-loader'
            ]
          }
        ]
      },
      plugins: [
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
          filename: "[name].css",
        }),
      ],
};